#!/usr/bin/make -f

# Turn on hardening options for compilation.
export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# Add an -O3 at the end to override the O2 we can't get rid of.
export DEB_CFLAGS_MAINT_APPEND = -O3
export DEB_CXXFLAGS_MAINT_APPEND = -O3

BUILDARCH := $(shell dpkg-architecture -qDEB_BUILD_ARCH)

# Compilation with debugging can use > 4G RAM, so disable it on most architectures.
ifneq ($(BUILDARCH),amd64)
DEB_CFLAGS_MAINT_APPEND += -g0
DEB_CXXFLAGS_MAINT_APPEND += -g0
endif

# Use debhelper.
%:
	dh $@ --buildsystem=meson

# Tell meson not to install the extra tools
override_dh_auto_configure:
	dh_auto_configure -- -Dextra-tools=false --buildtype=release -Db_ndebug=true

# Cleanup test artifacts scattered through the source code.
execute_after_dh_clean:
	find -type f -name \*.Main.hs -delete
	find -type f -name obtained-\* -delete
	find -type d -name ignore-output-\* | xargs rm -rf
